#include <iostream>
#include <vector>
#include <algorithm>
#include <queue>

using namespace std;

void sorting(vector <int>& atime, vector <int>& stime , int time) {
	int temp1,temp2, min;
	for (int i = 0; i < time - 1; i++) {
		min = i;
		for (int j = i + 1; j < time; j++)
			if (atime[j] < atime[min])
				min = j;
		if (min != i) {
			temp1 = atime[i];
			atime[i] = atime[min];
			atime[min] = temp1;

			temp2 = stime[i];
			stime[i] = stime[min];
			stime[min] = temp2;
		}
	}
}
void main() {
	queue <int> Queue;
	int MaxCustomers, MinCustomers, MaxServiceTime, MinServiceTime, NumCashiers; 
	cout << "Enter your MaxCustomers = "; //input
	cin >> MaxCustomers;
	cout << "Enter your MinCustomers = ";
	cin >> MinCustomers;
	cout << "Enter your MaxServiceTime = ";
	cin >> MaxServiceTime;
	cout << "Enter your MinServiceTime = ";
	cin >> MinServiceTime;
	cout << "Enter your NumCashiers = ";
	cin >> NumCashiers;
	vector <int> servicetime;
	vector <int> arrivedtime;
	int numCustomer;
	numCustomer = MaxCustomers - rand() % (MaxCustomers - MinCustomers + 1);
	cout << "Number of Customer = " << numCustomer << endl;
	for (int i = 0; i < numCustomer; i++) {    //random arrived time and servicetime
		int stime, atime;
		atime = 240 - rand() % (240 + 1);
		stime = MaxServiceTime - rand() % (MaxServiceTime - MinServiceTime + 1);
		arrivedtime.push_back(atime);
		servicetime.push_back(stime);
	}
	cout << endl;
	sorting(arrivedtime, servicetime, numCustomer); //sorting time
	for (int i = 0; i < numCustomer; i++) {
		Queue.push(arrivedtime[i]);
	}
	for (int i = 0; i < numCustomer; i++) { //add to queue
		if (Queue.empty()) {
			return;
		}
		else {
			cout << "customer" << i + 1 << " aTime = " << Queue.front() << " stime = " << servicetime[i] << endl; //show Time with sorted
			Queue.pop();
		}
	}
	for (int i = 0; i < numCustomer; i++) { //add to queue
		Queue.push(arrivedtime[i]);
	}
	int arrived_t = 0;
	int arrived_time[20] = {};
	int left_time[20] = {};
	int wait_time[20] = {};
	for (int i = 0; i < numCustomer; i + 0) {  //show arrived time, wait time, left time
		if (Queue.empty()) {
			return;
		}
		if (arrived_t == 0 && i == 0) {
			for (int c = 0; c < NumCashiers; c++) {
				arrived_time[c] = Queue.front();
				left_time[c] = arrived_time[c] + servicetime[c];
				wait_time[c] = 0;
				Queue.pop();
				cout << "customer" << i + 1 << " cashier = " << c + 1 << " arrived = " << arrived_time[c] << " wait time = " << wait_time[c] << " left time = " << left_time[c] << endl;
				i++;
				if (Queue.empty()) {
					return;
				}
			}
		}
		else {
			int n;
			int r = left_time[i];
			for (int c = 0; c < NumCashiers; c++) {
				if (r <= left_time[c]) {
					n = c;
					if (Queue.front() >= left_time[n]) {
						arrived_time[n] = Queue.front();
						wait_time[n] = 0;
					}
					else {
						wait_time[n] = left_time[n] - Queue.front();
						arrived_time[n] = left_time[n];
					}
					left_time[n] = arrived_time[n] + servicetime[i];
					Queue.pop();
					cout << "customer" << i + 1 << " cashier = " << c + 1 << " arrived = " << arrived_time[n] << " wait time = " << wait_time[n] << " left time = " << left_time[n] << endl;
					i++;
					if (Queue.empty()) {
						return;
					}
				}
			}
		}
	}
}